import scrapy
import csv
import datetime
import re
import json
from config import NET_IMOVEIS_START_URLS

API_URL = 'https://www.netimoveis.com/pesquisa'

VALUES = {
    'tipo': 'apartamento',
    'transacao': 'venda',
    'localizacao': [{"urlPais":"BR","urlEstado":"minas-gerais","urlCidade":"belo-horizonte","urlRegiao":"","urlBairro":"","urlLogradouro":"","idAgrupamento":"","tipo":"cidade","idLocalizacao":"BR-MG-belo-horizonte---"}],
    'pagina': '5',
    'retornaPaginacao': 'true',
    'outrasPags': 'true',
}

HEADER = {'x-requested-with': 'XMLHttpRequest'}

RESULTS_PER_PAGE = 42

class NetImoveisSpider(scrapy.Spider):
    name = "net_imoveis"
    start_urls = NET_IMOVEIS_START_URLS
    base_url = 'https://www.netimoveis.com'

    def parse(self, response):
        filters = response.css('input[name="filtros-url"]').xpath('@data-filter').get()
        data = json.loads(filters)
        params = {
            'transacao': data['transacao'],
            'localizacao': [{
                'urlPais': l['urlPais'],
                'urlEstado': l['urlEstado'],
                'urlCidade': l['urlCidade'],
                'urlRegiao': l['urlRegiao'],
                'urlBairro': l['urlBairro'],
                'urlLogradouro': l['urlLogradouro'],
                'tipo': l['tipo'],
                'idAgrupamento': l['idAgrupamento'],
                'idLocalizacao': l['idLocalizacao'],
            } for l in data['localizacao']],
            'pagina': 1,
            'retornaPaginacao': 'true',
            'outrasPags': 'true',            
        }
        if data['tipo']:
            params['tipo'] = data['tipo']
        params['localizacao'] = json.dumps(params['localizacao']).replace(' ', '')
        url = '%s?%s' % (API_URL, '&'.join(['%s=%s' % (k,v) for k,v in params.items()]))
        yield scrapy.Request(url=url, headers=HEADER, callback=self.parse_api, meta={'page': 1, 'params': params})

    def parse_api(self, response):
        data = json.loads(response.text)
        params = response.meta['params']
        if data['erro'] == False:
            if params['pagina'] * RESULTS_PER_PAGE < data['totalDeRegistros']:
                params['pagina'] = response.meta['page'] + 1
                url = '%s?%s' % (API_URL, '&'.join(['%s=%s' % (k,v) for k,v in params.items()]))
                meta = {'page': response.meta['page'] + 1, 'params': params}
                yield scrapy.Request(url=url, headers=HEADER, callback=self.parse_api, meta=meta)
            
            for imovel in data['lista']:
                meta = {
                    #'Site': 'netimoveis.com.br',
                    'Área da construção': str(float(imovel['areaConstruida'])),
                    'Quartos': str(int(imovel['quartos'])),
                    'Banheiros': str(int(imovel['banho'])),
                    'Vagas de garagem': str(int(imovel['vagasGaragem'])),
                    'Valor total': str(int(imovel.get('valorImovel', ''))),
                    'Valor locacao': str(int(imovel.get('valorLocacao', ''))),
                    'Venda': 'Sim' if imovel['transacaoVenda'] else 'Não',
                    'Locacao': 'Sim' if imovel['transacaoLocacao'] else 'Não',
                    'Bairro': imovel['nomeBairro'],
                    'Cidade': imovel['nomeCidade'],
                    'Endereço': '%s, %s' % (imovel['logradouroPublico'], imovel['complemento']),
                    'Código': imovel['imovelSan_Id'],
                    #'Link': '%s/%s' % (self.base_url, imovel['urlDetalheImovel']),
                    'tipoImovel1': imovel['tipoImovel1'],
                    'tipoImovel2': imovel['tipoImovel2'],

                }
                yield scrapy.Request(url='%s/%s' % (self.base_url, imovel['urlDetalheImovel']), callback=self.parse_item, meta=meta)

    
    def parse_item(self, response):
        item = {
            'Tipo': response.meta['tipoImovel1'],
            'Bairro': response.meta['Bairro'],
            'Endereço': response.meta['Endereço'],
            'Cidade': response.meta['Cidade'],
            'Informante': response.css('#h4-nomeAgencia::text').get(),
            'Telefone': response.css('.telefone-placeholder span::text').get(),
            'Área da construção': response.meta['Área da construção'],
            'Quartos': response.meta['Quartos'],
            'Banheiros': response.meta['Banheiros'],
            'Vagas de garagem': response.meta['Vagas de garagem'],
            'Valor total': response.meta['Valor total'],
            'Valor locacao': response.meta['Valor locacao'],
            'Venda': response.meta['Venda'],
            'Locacao': response.meta['Locacao'],
            'Site': 'netimoveis.com.br',
            'Link': response.url,
            'Código': response.meta['Código'],
            'NETIMOVEIS_tipoImovel1': response.meta['tipoImovel1'],
            'NETIMOVEIS_tipoImovel2': response.meta['tipoImovel2'],
        }

        item = {k:v.strip() for k,v in item.items() if isinstance(v, str)}

        yield item

