# Crawler Imóveis

Aplicação para baixar dados de imóveis da NetImóveis e VilaReal.

## Instalação

1. Instalar [python 3.10](https://www.python.org/ftp/python/3.10.10/python-3.10.10-amd64.exe).
2. Baixar o [Microsoft C++ Build Tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/).
3. Rodar o Microsoft C++ Build Tools e em Workloads selecionar e instalar o "Desktop development with C++".
4. Clicar duas vezes em instalar.bat.
5. Para rodar a aplicação, clique duas vezes em rodar.bat.

## Como usar

1. Edite o arquivo config.py.
2. Adicione ou remova os links que deseja crawlar nas variáveis _START_URLS, os links devem estar entre aspas e com vírgula no final. Linhas que começam com # são comentários e serão ignoradas pelo programa. Conforme o padrão abaixo:

```python
VIVA_REAL_START_URLS = [
    #'https://www.vivareal.com.br/venda/minas-gerais/belo-horizonte/',
    'https://www.vivareal.com.br/venda/minas-gerais/belo-horizonte/bairros/jardim-america/#onde=,Minas%20Gerais,Belo%20Horizonte,Bairros,Jardim%20Am%C3%A9rica,,,neighborhood,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EJardim%20America,,,;,Minas%20Gerais,Belo%20Horizonte,Bairros,Barreiro,,,neighborhood,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EBarreiro,,,',
    'https://www.vivareal.com.br/aluguel/minas-gerais/belo-horizonte/bairros/jardim-america/#onde=Brasil,Minas%20Gerais,Belo%20Horizonte,Bairros,Jardim%20Am%C3%A9rica,,,,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EJardim%20America,,,;,Minas%20Gerais,Belo%20Horizonte,Bairros,Barreiro,,,neighborhood,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EBarreiro,,,',
]

NET_IMOVEIS_START_URLS = [
    #'https://www.netimoveis.com/venda/minas-gerais/belo-horizonte?transacao=venda&localizacao=BR-MG-belo-horizonte---&pagina=1',
    'https://www.netimoveis.com/venda/minas-gerais/belo-horizonte/oeste/jardim-america?transacao=venda&localizacao=BR-MG-belo-horizonte-jardim-america-oeste-%2CBR-MG-belo-horizonte--barreiro-&pagina=1',
    'https://www.netimoveis.com/locacao/minas-gerais/belo-horizonte/oeste/jardim-america?transacao=locacao&localizacao=BR-MG-belo-horizonte-jardim-america-oeste-%2CBR-MG-belo-horizonte--barreiro-&pagina=1'
]
```
3. Clique duas vezes em rodar.bat.
4. Espere até o final da execução do programa.

## Autor
Bernardo Tavares - bhdpt3@gmail.com