from scrapy.dupefilters import RFPDupeFilter
from scrapy.http.request import Request
import hashlib

class SeenURLFilter(RFPDupeFilter):
    """A dupe filter that considers the URL"""

    def request_fingerprint(self, request: Request) -> str:
        return hashlib.sha1(request.url.encode('utf-8')).hexdigest()