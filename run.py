from config import SETTINGS
from scrapy.crawler import CrawlerProcess
from vivaRealSpider import VivaRealSpider
from netImoveisSpider import NetImoveisSpider

process = CrawlerProcess(settings=SETTINGS)
process.crawl(VivaRealSpider)
process.crawl(NetImoveisSpider)
process.start()