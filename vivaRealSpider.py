import scrapy
import csv
import datetime
import re
import json
from config import VIVA_REAL_START_URLS

API_URL = 'https://glue-api.vivareal.com/v2/listings'

VALUES = {
    'addressCity': 'Belo Horizonte',
    'addressLocationId': 'BR>Minas Gerais>NULL>Belo Horizonte>Barrios>Santo Antonio',
    'addressNeighborhood': 'Santo Antônio',
    'addressState': 'Minas Gerais',
    'addressCountry': 'Brasil',
    'addressStreet': '',
    'addressZone': 'Bairros',
    'addressPointLat': '-19.945038',
    'addressPointLon': '-43.944078',
    'business': 'SALE',
    'facets': 'amenities',
    'unitTypes': '',
    'unitSubTypes': '',
    'unitTypesV3': '',
    'usageTypes': '',
    'listingType': 'USED',
    'parentId': 'null',
    'categoryPage': 'RESULT',
    'includeFields': 'search(result(listings(listing(displayAddressType%2Camenities%2CusableAreas%2CconstructionStatus%2ClistingType%2Cdescription%2Ctitle%2CunitTypes%2CnonActivationReason%2CpropertyType%2CunitSubTypes%2Cid%2Cportal%2CparkingSpaces%2Caddress%2Csuites%2CpublicationType%2CexternalId%2Cbathrooms%2CusageTypes%2CtotalAreas%2CadvertiserId%2Cbedrooms%2CpricingInfos%2CshowPrice%2Cstatus%2CadvertiserContact%2CvideoTourLink%2CwhatsappNumber%2Cstamps)%2Caccount(id%2Cname%2ClogoUrl%2ClicenseNumber%2CshowAddress%2ClegacyVivarealId%2Cphones%2Ctier)%2Cmedias%2CaccountLink%2Clink))%2CtotalCount)%2Cpage%2CseasonalCampaigns%2CfullUriFragments%2Cnearby(search(result(listings(listing(displayAddressType%2Camenities%2CusableAreas%2CconstructionStatus%2ClistingType%2Cdescription%2Ctitle%2CunitTypes%2CnonActivationReason%2CpropertyType%2CunitSubTypes%2Cid%2Cportal%2CparkingSpaces%2Caddress%2Csuites%2CpublicationType%2CexternalId%2Cbathrooms%2CusageTypes%2CtotalAreas%2CadvertiserId%2Cbedrooms%2CpricingInfos%2CshowPrice%2Cstatus%2CadvertiserContact%2CvideoTourLink%2CwhatsappNumber%2Cstamps)%2Caccount(id%2Cname%2ClogoUrl%2ClicenseNumber%2CshowAddress%2ClegacyVivarealId%2Cphones%2Ctier)%2Cmedias%2CaccountLink%2Clink))%2CtotalCount))%2Cexpansion(search(result(listings(listing(displayAddressType%2Camenities%2CusableAreas%2CconstructionStatus%2ClistingType%2Cdescription%2Ctitle%2CunitTypes%2CnonActivationReason%2CpropertyType%2CunitSubTypes%2Cid%2Cportal%2CparkingSpaces%2Caddress%2Csuites%2CpublicationType%2CexternalId%2Cbathrooms%2CusageTypes%2CtotalAreas%2CadvertiserId%2Cbedrooms%2CpricingInfos%2CshowPrice%2Cstatus%2CadvertiserContact%2CvideoTourLink%2CwhatsappNumber%2Cstamps)%2Caccount(id%2Cname%2ClogoUrl%2ClicenseNumber%2CshowAddress%2ClegacyVivarealId%2Cphones%2Ctier)%2Cmedias%2CaccountLink%2Clink))%2CtotalCount))%2Caccount(id%2Cname%2ClogoUrl%2ClicenseNumber%2CshowAddress%2ClegacyVivarealId%2Cphones%2Ctier%2Cphones)%2Cdevelopments(search(result(listings(listing(displayAddressType%2Camenities%2CusableAreas%2CconstructionStatus%2ClistingType%2Cdescription%2Ctitle%2CunitTypes%2CnonActivationReason%2CpropertyType%2CunitSubTypes%2Cid%2Cportal%2CparkingSpaces%2Caddress%2Csuites%2CpublicationType%2CexternalId%2Cbathrooms%2CusageTypes%2CtotalAreas%2CadvertiserId%2Cbedrooms%2CpricingInfos%2CshowPrice%2Cstatus%2CadvertiserContact%2CvideoTourLink%2CwhatsappNumber%2Cstamps)%2Caccount(id%2Cname%2ClogoUrl%2ClicenseNumber%2CshowAddress%2ClegacyVivarealId%2Cphones%2Ctier)%2Cmedias%2CaccountLink%2Clink))%2CtotalCount))%2Cowners(search(result(listings(listing(displayAddressType%2Camenities%2CusableAreas%2CconstructionStatus%2ClistingType%2Cdescription%2Ctitle%2CunitTypes%2CnonActivationReason%2CpropertyType%2CunitSubTypes%2Cid%2Cportal%2CparkingSpaces%2Caddress%2Csuites%2CpublicationType%2CexternalId%2Cbathrooms%2CusageTypes%2CtotalAreas%2CadvertiserId%2Cbedrooms%2CpricingInfos%2CshowPrice%2Cstatus%2CadvertiserContact%2CvideoTourLink%2CwhatsappNumber%2Cstamps)%2Caccount(id%2Cname%2ClogoUrl%2ClicenseNumber%2CshowAddress%2ClegacyVivarealId%2Cphones%2Ctier)%2Cmedias%2CaccountLink%2Clink))%2CtotalCount))',
    'size': '36',
    'from': '0',
    'q': '',
    'developmentsSize': '5',
    '__vt': '',
    'levels': 'NEIGHBORHOOD',
    'ref': '',
    'pointRadius': '',
    'isPOIQuery': '',
}

HEADER = {
    'x-domain': 'www.vivareal.com.br',
    'accept': 'application/json'
}

METADATA_REGEX = r'fullUriFragments":(\{.*?parentId.*?\})'


class VivaRealSpider(scrapy.Spider):
    name = "viva_real"
    start_urls = VIVA_REAL_START_URLS
    base_url = 'https://www.vivareal.com.br'
    total_count = False

    def parse(self, response):
        # for page in response.css('ul.pagination__wrapper li.pagination__item button.js-change-page').xpath('@data-page').getall():
        #     if page.isnumeric():
        #         pattern = r'pagina=\d+'
        #         if re.search(pattern, response.url):
        #             url = re.sub(pattern, 'pagina=%s' % page, response.url)
        #         else:
        #             url = '%s%s%s' % (response.url, '?pagina=', page)
        #         yield scrapy.Request(url=url, callback=self.parse)

        # for url in response.css('.results-list .property-card__container a').xpath('@href').getall():
        #     yield scrapy.Request(url=self.base_url+url, callback=self.parse_item)

        page_metadata_json = response.css('script::text').re_first(METADATA_REGEX)
        page_metadata = json.loads(page_metadata_json)
        address = page_metadata['addresses'][0]

        params = VALUES.copy()

        params['addressState'] = address.get('state', '')
        params['addressCity'] = address.get('city', '')
        params['addressLocationId'] = address.get('locationId', '')
        params['addressNeighborhood'] = address.get('neighborhood', '')
        params['addressZone'] = address.get('zone', '')
        params['addressPointLat'] = address['point']['lat']
        params['addressPointLon'] = address['point']['lon']
        params['business'] = page_metadata.get('business', '')
        params['parentId'] = page_metadata.get('parentId', '')
        params['listingType'] = page_metadata.get('listingType', '')
        #params['unitTypes'] = ','.join(response.css('.select-multiple__list-item checkbox').xpath('@value').getall())
        #print(response.css('.filters-panel__main-filters .select-multiple__list-item input').xpath('@value').getall())
        # params['unitSubTypes'] = page_metadata['unitSubTypes']
        # params['unitTypesV3'] = page_metadata['unitTypesV3']
        # params['usageTypes'] = page_metadata['usageTypes']

        params['from'] = 0
        url = '%s?%s' % (API_URL, '&'.join(['%s=%s' % (k,v) for k,v in params.items()]))
        yield scrapy.Request(url=url, headers=HEADER, callback=self.parse_api, meta={'params': params})

    def parse_api(self, response):
        data = json.loads(response.text)
        params = response.meta['params']
        developments = []
        if data.get('developments', False):
            developments = data['developments']['search']['result']['listings']
        search = data['search']['result']['listings']
        total_count = data['page']['uriPagination']['total']
        page = data['page']['uriPagination']['page']
        size = data['page']['uriPagination']['size']
        if page * size < total_count:
            params['from'] = page * size
            url = '%s?%s' % (API_URL, '&'.join(['%s=%s' % (k,v) for k,v in params.items()]))
            yield scrapy.Request(url=url, headers=HEADER, callback=self.parse_api, meta={'params': params})

        for listing in developments+search:
            url = listing['link']['href']
            meta = {
                'usageTypes': ', '.join(listing['listing']['usageTypes']),
                'unitTypes': ', '.join(listing['listing']['unitTypes'])
            }
            yield scrapy.Request(url=self.base_url+url, callback=self.parse_item, meta=meta)

        
        

    def parse_item(self, response):
        address = response.css('.title__address::text').get()
        sale_rent = response.css('.price__title::text').get().strip()
        value = response.css('.price__price-info::text').get().replace('/mês', '').strip()
        item = {
            'Tipo': response.meta['unitTypes'],
            'Bairro': ', '.join(address.split(', ')[:-1]).split(' - ')[-1] if address else '',
            'Endereço': ', '.join(address.split(', ')[:-1]) if address else '',
            'Cidade': address.split(', ')[-1] if address else '',
            #'Informante': response.css('publisher__name::text').get(),
            # 'Telefone',
            'Área da construção': response.css('.features__item--area::text').get(),
            'Quartos': response.css('.features__item--bedroom::text').get(),
            'Banheiros': response.css('.features__item--bathroom::text').get(),
            'Vagas de garagem': response.css('.features__item--parking::text').get(),
            'Valor total': value if sale_rent == 'Compra' else 0,
            'Valor locacao': value if sale_rent == 'Aluguel' else 0,
            'Venda': 'Sim' if sale_rent == 'Compra' else 'Não',
            'Locacao': 'Sim' if sale_rent == 'Aluguel' else 'Não',
            'Site': 'vivareal.com.br',
            'Link': response.url,
            'Código': response.css('.title__code::text').get(),
            'VIVAREAL_usageTypes': response.meta['usageTypes'],
            'VIVAREAL_unitTypes': response.meta['unitTypes'],
        }
        phones_pattern = r'\{"phones":\[.*?\]\}'
        phones_json = response.css('script::text').re_first(phones_pattern)
        if phones_json:
            phones = json.loads(phones_json)
            item['Telefone'] = ' / '.join(phones['phones'])

        advertiser_pattern = r'"advertiserLinks":\[\{"data":\{\},"name":"(.*?)"\,"hre'
        advertiser = response.css('script::text').re_first(advertiser_pattern)
        if advertiser:
            item['Informante'] = advertiser

        item = {k:v.strip() for k,v in item.items() if isinstance(v, str)}

        #Valor Total para int
        if isinstance(item.get('Valor total'), str):
            price = item['Valor total'].replace('R$', '').replace('.','').replace(',','').strip()
            if price.isnumeric():
                item['Valor total'] = int(price)
            else:
                item['Valor total'] = ''

        #Valor Locacao para int
        if isinstance(item.get('Valor locacao'), str):
            price = item['Valor locacao'].replace('R$', '').replace('.','').replace(',','').strip()
            if price.isnumeric():
                item['Valor locacao'] = int(price)
            else:
                item['Valor locacao'] = ''
        
        yield item

