VIVA_REAL_START_URLS = [
    #'https://www.vivareal.com.br/venda/minas-gerais/belo-horizonte/',
    'https://www.vivareal.com.br/venda/minas-gerais/belo-horizonte/bairros/jardim-america/#onde=,Minas%20Gerais,Belo%20Horizonte,Bairros,Jardim%20Am%C3%A9rica,,,neighborhood,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EJardim%20America,,,;,Minas%20Gerais,Belo%20Horizonte,Bairros,Barreiro,,,neighborhood,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EBarreiro,,,',
    'https://www.vivareal.com.br/aluguel/minas-gerais/belo-horizonte/bairros/jardim-america/#onde=Brasil,Minas%20Gerais,Belo%20Horizonte,Bairros,Jardim%20Am%C3%A9rica,,,,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EJardim%20America,,,;,Minas%20Gerais,Belo%20Horizonte,Bairros,Barreiro,,,neighborhood,BR%3EMinas%20Gerais%3ENULL%3EBelo%20Horizonte%3EBarrios%3EBarreiro,,,',
]

NET_IMOVEIS_START_URLS = [
    #'https://www.netimoveis.com/venda/minas-gerais/belo-horizonte?transacao=venda&localizacao=BR-MG-belo-horizonte---&pagina=1',
    'https://www.netimoveis.com/venda/minas-gerais/belo-horizonte/oeste/jardim-america?transacao=venda&localizacao=BR-MG-belo-horizonte-jardim-america-oeste-%2CBR-MG-belo-horizonte--barreiro-&pagina=1',
    'https://www.netimoveis.com/locacao/minas-gerais/belo-horizonte/oeste/jardim-america?transacao=locacao&localizacao=BR-MG-belo-horizonte-jardim-america-oeste-%2CBR-MG-belo-horizonte--barreiro-&pagina=1'
]

FILEPATH = 'data/imoveis-data_%s.csv'

SETTINGS = {
    'LOG_LEVEL': 'INFO',
    'AUTOTHROTTLE_ENABLED': True,
    # 'CONCURRENT_REQUESTS_PER_DOMAIN': 2,
    # 'CONCURRENT_REQUESTS': 10,
    # 'CONCURRENT_ITEMS': 20,
    'REACTOR_THREADPOOL_MAXSIZE': 50,
    'DOWNLOAD_DELAY': 0.5,
    'ROBOTSTXT_OBEY': False,
    'RETRY_TIMES': 10,
    'DUPEFILTER_CLASS': 'dupefilter.SeenURLFilter',
    'URLLENGTH_LIMIT': 5000,
    'USER_AGENT': 'Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148',
    'ITEM_PIPELINES': {
        'csvPipeline.CsvManager': 300,
    }
}