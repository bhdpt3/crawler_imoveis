from config import FILEPATH
import csv
import datetime

HEADER = [
    'Tipo',
    'Bairro',
    'Endereço',
    'Cidade',
    'Informante',
    'Telefone',
    'Área da construção',
    'Quartos',
    'Banheiros',
    'Vagas de garagem',
    'Valor total',
    'Valor locacao',
    'Venda',
    'Locacao',
    'Site',
    'Link',
    'Código',
    'VIVAREAL_usageTypes',
    'VIVAREAL_unitTypes',
    'NETIMOVEIS_tipoImovel1',
    'NETIMOVEIS_tipoImovel2',
]

filename = FILEPATH % datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
csvfile = open(filename, 'w', newline='')
csvWriter = csv.DictWriter(csvfile, fieldnames=HEADER, quotechar='"', delimiter=',',
                            quoting=csv.QUOTE_ALL, skipinitialspace=True)
csvWriter.writeheader()

class CsvManager:

    def __init__(self):
        self.csvWriter = csvWriter

    def save(self, data):
        self.csvWriter.writerow(data)

    def process_item(self, item, spider):
        self.save(item)